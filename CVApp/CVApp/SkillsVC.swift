//
//  SkillsVC.swift
//  CVApp
//
//  Created by Henric Hiljanen on 2018-12-07.
//  Copyright © 2018 HenricHiljanen. All rights reserved.
//

import UIKit

class SkillsVC: UIViewController {

    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        button.backgroundColor = UIColor.blue
        button.setTitleColor(UIColor.white, for: .normal)
        
        UIView.animate(withDuration: 4) {
            self.view.backgroundColor = UIColor.blue
            self.button.backgroundColor = UIColor.white
            self.button.setTitleColor(UIColor.blue, for: .normal)
            
            self.button.layer.cornerRadius = self.button.frame.size.width / 2
            self.button.setTitle("HELLO", for: .normal)
        }

        // Do any additional setup after loading the view.
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
