//
//  ExperienceVC.swift
//  CVApp
//
//  Created by Henric Hiljanen on 2018-12-07.
//  Copyright © 2018 HenricHiljanen. All rights reserved.
//

import UIKit

class ExperienceVC: UITableViewController {
    
    var exp: Experience = Experience()
    var sections = ["Work Experience", "Education"]
    var experiences: [[Experience]] = [
        [Experience(imageName: "microscope", title: "Work 1", timeSpan: "2013-2014", description: WorkText().getText(title: "Work 1")),
         Experience(imageName: "internet", title: "Work 2", timeSpan: "2014-2017", description: WorkText().getText(title: "Work 2")),
         Experience(imageName: "test", title: "Work 3", timeSpan: "2017-2018", description: WorkText().getText(title: "Work 3"))],
        [Experience(imageName: "web", title: "Work 4", timeSpan: "2018-current", description: WorkText().getText(title: "Work 4"))]
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ExperienceDetailVC {
            let selectedExperience = exp
            destination.experience = selectedExperience
        }
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.sections.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sections[section]
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section) {
        case 0:
            return experiences[0].count
        case 1:
            return experiences[1].count
        default:
            return 0
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cellProto1", for: indexPath) as? ExperienceCell {
            
            let experience = experiences[indexPath.section][indexPath.row]
            
            cell.ImageView.image = UIImage(named: experience.imageName)
            cell.titleLabel.text = experience.title
            cell.timeSpanLabel.text = experience.timeSpan
            
            return cell
        } else {
            return UITableViewCell()
        }
        
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        exp = experiences[indexPath.section][indexPath.row]
        
        performSegue(withIdentifier: "toExpDetailSegue", sender: self)
    }
    
}
