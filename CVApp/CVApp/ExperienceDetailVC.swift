//
//  ExperienceDetailVC.swift
//  CVApp
//
//  Created by Henric Hiljanen on 2018-12-07.
//  Copyright © 2018 HenricHiljanen. All rights reserved.
//

import UIKit

class ExperienceDetailVC: UIViewController {

    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var navTitle: UINavigationItem!
    var experience: Experience = Experience()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = UIImage(named: experience.imageName+"BIG")
        titleLabel.text = experience.title
        timeLabel.text = experience.timeSpan
        textLabel.text = experience.description
        navTitle.title = experience.title
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
